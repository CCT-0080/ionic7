// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic','ngCordova']);

var db = null;

app.config(function($stateProvider,$urlRouterProvider){
  $stateProvider
  .state('cadastro',{
    url:'/cadastro',
    templateUrl:'cadastro.html',
    controller:'ctrl'
  })
  .state('listar',{
    cache: false,
    url:'/listar',
    templateUrl:'listar.html',
    controller:'ctrl'
  });
  $urlRouterProvider.otherwise('/cadastro');
});


app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if (window.cordova && window.Keyboard) {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }
    try {
      db = window.openDatabase('teste.db','1.0','teste.db',65536);
    } catch(e){
      console.log("Erro ao criar o banco de dados!!");
    }
    try {
      db.transaction(function(tx){
        tx.executeSql("CREATE TABLE IF NOT EXISTS pessoas id integer primary key autoincrement, firstname text, lastname text)",[],function(tx,res){
           console.log("Funcionou!!");
        },function(error){
           console.log("Erro: "+error);
        });
      });
    } catch (e){
      console.log("Erro fatal");
    }
  });
});

app.controller('ctrl',function($scope,$cordovaDialogs,$cordovaToast){

   $scope.data={};

   $scope.inserir = function(){
    var nome = $scope.data.nome;
    var sobrenome = $scope.data.sobrenome;
    if (angular.isDefined(nome) && angular.isDefined(sobrenome)) {
      try {
          db.transaction(function(tx){
              tx.executeSql("INSERT INTO pessoas(firstname,lastname) VALUES(?,?)",
              [nome,sobrenome],function(tx,res){  
          } ,function(error){
          console.log("Erro ao inserir os dados "+error);
          });
        }); 
      } catch (e){
        console.log("Não foi possível inserir os dados");
      } 
    }
    $scope.data = {};
   }; 
   
   $scope.excluirTodos = function(){
     try {
      db.transaction(function(tx){
        tx.executeSql("DELETE FROM pessoas",[],function(tx,res){
          console.log("Elementos excluidos com sucesso!!");
        },function(error){
          console.log(error);
        });
      });
     } catch (e) {
       console.log("Não foi possível excluir todos os dados");
     }
   };

   $scope.excluir = function(id){
    try {
      db.transaction(function(tx){
        tx.executeSql("DELETE FROM pessoas where id=?",[id],function(tx,res){
          console.log("Elemento excluido com sucesso!!");
        },function(error){
          console.log(error);
        });
      });
    } catch (e) {
      console.log("Não foi possivel excluir");
    }
    $scope.listarTodos();
   };

   $scope.detalhar = function(id){
     try {
       tx.executeSql("Select * from pessoas where id=?",[id],function(tx,res){
          if (res.rows.length != 0){
            $scope.result.push(res.rows.item(0));
            console.log(res.rows.item(0));
          }  
       },function(error){
          console.log(error);
       });
     } catch (e) {
       console.log("Não foi possível localizar a pessoa");
     }
   }

   $scope.listarTodos = function(){

    try {
      db.transaction(function(tx){
        tx.executeSql("SELECT * FROM pessoas",[],function(tx,res){
          $scope.pessoas = [];
          if (res.rows.length != 0){ 
            for (var i=0; i < res.rows.length; i++){
              $scope.pessoas.push(res.rows.item(i));
            } 
          }
        },function(error){
          console.log(error);
        })
      });
    } catch(e){
      console.log("Não foi possível listar");
    }
   };

   $scope.listarTodos();

});

